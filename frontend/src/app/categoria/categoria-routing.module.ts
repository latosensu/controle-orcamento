import { CadastroCategoriaComponent } from './cadastro-categoria/cadastro-categoria.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriaComponent } from './categoria.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CategoriaComponent
  },
  {
    path: 'cadastro',
    pathMatch: 'full',
    component: CadastroCategoriaComponent
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
  ]
})
export class CategoriaRoutingModule { }
