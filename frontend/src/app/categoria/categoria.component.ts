import { CategoriaApiService } from './../core/categoria-api.service';
import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { CategoriaDataSource} from './categoria-datasource';
import { Categoria } from '../core/model/categoria';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss']
})
export class CategoriaComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatTable) table!: MatTable<Categoria>;
  dataSource?: CategoriaDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'nome', 'tipo', 'descricao'];

  constructor(private categoriaApiService: CategoriaApiService, private router: Router) {

  }

  ngAfterViewInit(): void {
    this.categoriaApiService.listAll().then((result) => {
      this.dataSource = new CategoriaDataSource(result);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
    });
  }

  adicionarCategoria = () => {
    this.router.navigateByUrl('categoria/cadastro');
  }
}
