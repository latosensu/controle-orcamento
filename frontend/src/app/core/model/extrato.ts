import { Lancamento } from "./lancamento";

export interface Extrato {
  lancamentos: Lancamento[]
  saldo: number,
  dataInicial: string,
  dataFinal: string,
  resultadoPeriodo: number,
}
