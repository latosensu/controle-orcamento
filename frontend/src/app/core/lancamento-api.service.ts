import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Categoria } from './model/categoria';
import { Lancamento } from './model/lancamento';

@Injectable({
  providedIn: 'root'
})
export class LancamentoApiService {

  constructor(private httpClient: HttpClient) { }

  // listAll = (): Promise<Categoria[]> => {

  //   return this.httpClient.get<Categoria[]>(environment.apiUrl + '/categoria').toPromise();
  // }

  adicionarLancamento = (descricao: string, categoria: Categoria, valor: number) => {
    return this.httpClient.post<Lancamento>(
      environment.apiUrl + '/lancamento',
      {
        descricao: descricao,
        categoria: categoria,
        valor: valor
      } as Lancamento).toPromise();

  }
}
