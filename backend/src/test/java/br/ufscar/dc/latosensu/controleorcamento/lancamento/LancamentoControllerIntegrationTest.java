package br.ufscar.dc.latosensu.controleorcamento.lancamento;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.ufscar.dc.latosensu.controleorcamento.ControleOrcamentoApplication;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria.Categoria;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria.CategoriaRepository;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria.TipoCategoria;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento.Lancamento;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento.LancamentoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.MOCK,
  classes = ControleOrcamentoApplication.class
)
@AutoConfigureMockMvc
@TestPropertySource(
  locations = "classpath:application-integrationtest.properties"
)
public class LancamentoControllerIntegrationTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private LancamentoRepository<Lancamento, Long> lancamentoRepository;

  @Autowired
  private CategoriaRepository categoriaRepository;

  @Test
  public void dadoUmLancamento_quandoObtemLancamentos_oLancamentoEhRetornado() throws Exception {
    criarLancamento("bob");

    mvc
      .perform(
        get("/lancamento").contentType(MediaType.APPLICATION_JSON)
      )
      .andExpect(status().isOk())
      .andExpect(
        content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)
      )
      .andExpect(jsonPath("$[0].descricao", is("Meu salário")));
  }

  private void criarLancamento(String string) {
    Categoria categoria = new Categoria();
    categoria.setNome("Salário");
    categoria.setTipo(TipoCategoria.ENTRADA);
    categoria.setDescricao("Recebimento de Salário");

    categoria = categoriaRepository.save(categoria);
    Lancamento lancamento = new Lancamento();
    lancamento.setCategoria(categoria);
    lancamento.setValor(BigDecimal.valueOf(54.6));
    lancamento.setDataBalancete(LocalDate.now());
    lancamento.setDescricao("Meu salário");
    lancamento = lancamentoRepository.save(lancamento);
  }
}
