package br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * Interface para persistência e recuperação de categorias no banco de dados
 */
public interface CategoriaRepository extends PagingAndSortingRepository<Categoria, Long>{
    List<Categoria> findByNome(@Param("nome") String nome);

    @Override
    List<Categoria> findAll();
}
