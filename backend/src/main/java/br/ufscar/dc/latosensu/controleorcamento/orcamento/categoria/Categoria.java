package br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;


/**
 * Classe usada para representar uma categoria de movimentação, que pode ser tanto de entrada, quanto de saída.
 */
@Data
@Entity
public class Categoria {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull(message = "É necessário informar um nome")
    @Size(min = 3, max = 50)
    private String nome;
    @NotNull(message = "É necessário informar uma descrição")
    @Size(min = 3, max = 256)
    private String descricao;

    @Override
    public String toString() {
      return "Categoria [descricao=" + descricao + ", id=" + id + ", nome=" + nome + ", tipo=" + tipo + "]";
    }

    @NotNull(message = "É necessário informar um tipo")
    private TipoCategoria tipo;
}



